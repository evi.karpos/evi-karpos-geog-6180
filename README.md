# Installation
Git clone using the following command:
`git clone git@gitlab.com:evi.karpos/evi-karpos-geog-6180.git`

# Required Packages
`numpy`, `matplotlib.pyplot`, `pandas`, `glob`, `os`, `sys`, `imageio`, `geopandas`, `psycopg2`

# GeoPandas Problems
`Geopandas` may fail to install using `conda` and `pip`, it is suggested to use a clean environment when running this. i.e., run this code:

```
conda create --name evi_karpos_spatial_project
conda activate evi_karpos_spatial_project
```

After the environment is created, `conda install` all the packages above.

# How to run this code
1. Open Juptyer notebook file titled "GEOG Final Project.ipynb" to see fully run code, OR,
2. Clone repo and run step by step and follow above directions to create the environment. 

# To view the GIF
Select the file titled "timeseries.gif" 
